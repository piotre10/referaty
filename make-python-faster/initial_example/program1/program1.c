#include<stdio.h>
#include<stdlib.h>

int main() {
  int N = 1000000;
  double x0 = -4.9;
  double rx = 0.00013;
  double y0 = 17.42;
  double ry = -0.00007; 
  
  double *x = (double*) calloc(N, sizeof(double));
  if(x == NULL) {
    printf("Out of memory error: could not allocate x");
    return 1;
  }

  double *y = (double*) calloc(N, sizeof(double));
  if(y == NULL) {
    printf("Out of memory error: could not allocate y");
    return 1;
  }

  for(int i=0; i<N; i++){
    x[i] = x0 + rx*i;
    y[i] = y0 + ry*i;
  }
  
  double result = 0;

  for(int i=0; i<N; i++){
    result += x[i]*y[i];
  }

  printf("Calculated result: %f\n", result);
  
  free(x);
  free(y);
  return 0;
}
