N = 1000000
x0 = -4.9
rx = 0.00013
x_end = x0+(N-1)*rx
y0 = 17.42
ry = -0.00007
y_end = y0+(N-1)*ry

x = seq(x0, x_end, by=rx)
y = seq(y0, y_end, by=ry)

s=0
for (i in 1:N) {
  s = s + x[i]*y[i]
}

print(sprintf("Calculated Result: %f", s))
