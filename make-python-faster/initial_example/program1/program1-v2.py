N = 1000000
x0 = -4.9
rx = 0.00013
y0 = 17.42
ry = -0.00007

x = [x0+n*rx for n in range(N)]
y = [y0+n*ry for n in range(N)]

s = 0

for xi, yi in zip(x, y):
    s += xi*yi

print(f"Calculated result: {s}")
