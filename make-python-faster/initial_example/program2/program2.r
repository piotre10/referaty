N = 1000000
x0 = -4.9
rx = 0.00013
y0 = 17.42
ry = -0.00007

s=0
xi=x0
yi=y0
for (i in 1:N) {
  s = s + xi*yi
  xi = xi + rx
  yi = yi + ry
}

print(sprintf("Calculated Result: %f", s))
