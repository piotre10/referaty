N = 1000000
x0 = -4.9
rx = 0.00013
y0 = 17.42
ry = -0.00007


xi=x0
yi=y0
res=0
# I know that it will overwrite x0 and y0
for _ in range(N):
    res += xi*yi
    xi += rx
    yi += ry

print(f"Calculated result: {res}")
