#include<stdio.h>
#include<stdlib.h>

int main() {
  int N = 1000000;
  double x0 = -4.9;
  double rx = 0.00013;
  double y0 = 17.42;
  double ry = -0.00007; 

  double xi = x0;
  double yi = y0;
  double res = 0;
  
  for(int i=0; i<N; i++){
    res += xi*yi;
    xi += rx;
    yi += ry;
  }
 
  printf("Calculated result: %f\n", res);
  return 0;
}
  
