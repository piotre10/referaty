import numpy as np

N = 1000000
x0 = -4.9
rx = 0.00013
x_end = x0 + (N)*rx
y0 = 17.42
ry = -0.00007
y_end = y0 + (N)*ry

x = np.arange(x0, x_end, rx)
y = np.arange(y0, y_end, ry)

res = x @ y
print(f'Calculated result: {res}')

