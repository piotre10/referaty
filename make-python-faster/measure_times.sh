echo "real, user, sys" > $3
seq $2 | xargs -I -- time -f "%e, %U, %S" -a -o $3 $1 > /dev/null
