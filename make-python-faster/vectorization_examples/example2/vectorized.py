import numpy as np

N = 1000000
k=4

x1 = np.random.rand(N)
temp = (x1 < 1/2)*x1
longer_segment = np.max(np.vstack((x1, 1-x1)), axis=0)
x2 = temp + np.random.rand(N)*longer_segment

t1 = np.min(np.vstack((x1, x2)), axis=0)
t2 = np.abs(x1-x2)
t3 = 1 - t1 - t2

longest_segments = np.max(np.vstack((t1,t2,t3)), axis=0)
s = (longest_segments < 1/2).sum()

print(f"Estimated: {s / N}")
