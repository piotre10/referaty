import random

N = 1000000
k = 4

Z = []
for i in range(N):
    zi = 0
    for j in range(k*i, k*(i+1)):
        xj = random.gauss(0, 1)
        yj = random.random()/(j+1)
        zi += xj*yj

    Z.append(zi)
