import random

N = 1000000

s = 0

for _ in range(N):
    x1 = random.random()
    if x1 < 1/2:
        x2 = x1 + random.random()*(1-x1)
    else:
        x2 = random.random()*x1

    t1 = min(x1, x2)
    t2 = abs(x2-x1)
    t3 = 1-t1-t2
    if max(t1, t2, t3) < 1/2:
        s += 1

estimated_prob = s / N

print(f"Estimated prob is {estimated_prob}")
