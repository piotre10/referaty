# referaty
Repository for presentations and codes from conferrences

## Index

### [Make Python Faster](./make-python-faster/)
#### Description
Presentation about shortcomings of python and their origins, comparing to other to C and R programming languages with examples on how to make data analysis and simulations faster in python with numpy and vectorization of problems.
#### Conferences
- [Elements 2023](https://elements.agh.edu.pl/)
